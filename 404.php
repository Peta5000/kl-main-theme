<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

	<div id="primary" class="site-content">
		<div id="content" role="main">

			<article id="post-0" class="post error404 no-results not-found">
				<br/>
				<header class="entry-header">
					<h1 class="entry-title">Seite nicht gefunden</h1>
				</header>

				<div class="entry-content">
					<p>Die von Ihnen aufgerufene Seite konnte nicht gefunden werden.</p>
					<p>Bitte überprüfen Sie die URL in der Adresszeile.</p>
					<a href="http://www.krohn-leitmannstetter.de">Zurück zur Startseite</a>
				</div><!-- .entry-content -->
			</article><!-- #post-0 -->

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>
<?php
/**
 * Template Name: Welcome Page Template
 *
 * Description: A page template that provides a welcome page showing a selection menu for micro sites.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
$language_param = filter_input(INPUT_GET, "lang", FILTER_SANITIZE_STRING);
$language = "de";
if ($language_param && $language_param == "en") {
    $language = "en";
}
// Advanced Custom Fields
// WILLKOMMEN
$willkommen_bild_fur_hauptseite = get_field('allgemein_willkommen_bild_fur_hauptseite');
$willkommen_link_fur_hauptseite = get_field('allgemein_willkommen_link_fur_hauptseite');
$willkommen_bild_fur_therapie = get_field('allgemein_willkommen_bild_fur_therapie');
$willkommen_link_fur_therapie = get_field('allgemein_willkommen_link_fur_therapie');
$willkommen_bild_fur_pflege = get_field('allgemein_willkommen_bild_fur_pflege');
$willkommen_link_fur_pflege = get_field('allgemein_willkommen_link_fur_pflege');
$willkommen_bild_fur_news = get_field('allgemein_willkommen_bild_fur_news');
$willkommen_link_fur_news = get_field('allgemein_willkommen_link_fur_news');
$willkommen_bild_fur_karriere = get_field('allgemein_willkommen_bild_fur_karriere');
$willkommen_link_fur_karriere = get_field('allgemein_willkommen_link_fur_karriere');
$willkommen_bild_fur_philosophie = get_field('allgemein_willkommen_bild_fur_philosophie');
$willkommen_link_fur_philosophie = get_field('allgemein_willkommen_link_fur_philosophie');
$willkommen_link_zum_impressum = get_field('allgemein_willkommen_link_zum_impressum');

$willkommen_uberschrift = get_field($language . '_willkommen_uberschrift');
$willkommen_text_fur_hauptseite = get_field($language . '_willkommen_text_fur_hauptseite');
$willkommen_beschreibung_fur_hauptseite = get_field($language . '_willkommen_beschreibung_fur_hauptseite');
$willkommen_text_fur_therapie = get_field($language . '_willkommen_text_fur_therapie');
$willkommen_beschreibung_fur_therapie = get_field($language . '_willkommen_beschreibung_fur_therapie');
$willkommen_text_fur_pflege = get_field($language . '_willkommen_text_fur_pflege');
$willkommen_beschreibung_fur_pflege = get_field($language . '_willkommen_beschreibung_fur_pflege');
$willkommen_text_fur_news = get_field($language . '_willkommen_text_fur_news');
$willkommen_beschreibung_fur_news = get_field($language . '_willkommen_beschreibung_fur_news');
$willkommen_text_fur_karriere = get_field($language . '_willkommen_text_fur_karriere');
$willkommen_beschreibung_fur_karriere = get_field($language . '_willkommen_beschreibung_fur_karriere');
$willkommen_text_fur_philosophie = get_field($language . '_willkommen_text_fur_philosophie');
$willkommen_beschreibung_fur_philosophie = get_field($language . '_willkommen_beschreibung_fur_philosophie');
$willkommen_copyright = get_field($language . '_willkommen_copyright');
$willkommen_impressum = get_field($language . '_willkommen_impressum');
?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
    <meta charset="<?php bloginfo('charset'); ?>"/>
    <meta name="viewport" content="width=device-width"/>
    <title>Willkommen bei Krohn-Leitmannstetter</title>
    <link rel="profile" href="http://gmpg.org/xfn/11"/>
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>
    <?php // Loads HTML5 JavaScript file to add support for HTML5 elements in older IE versions. ?>
    <!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
    <?php wp_head(); ?>
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/welcome-page-style.css"/>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
    <header>
        <!--
        <div>
            <a href="<?php bloginfo('url'); ?>/willkommen" class="<?php echo ($language == 'de' ? 'welcome-selected-language' : '')?>">de</a>
            &nbsp;|&nbsp;
            <a href="<?php bloginfo('url'); ?>/willkommen?lang=en" class="<?php echo ($language == 'en' ? 'welcome-selected-language' : '')?>">en</a>
        </div>
        -->
        <div class="welcome-header-text">
            <?php echo $willkommen_uberschrift ?>
        </div>
    </header>
    <div id="main" class="wrapper">
        <a href="<?php echo $willkommen_link_fur_hauptseite ?>" class="welcome-teaser-container">
            <div class="welcome-teaser">
                <img class="welcome-teaser-image"
                     src="<?php echo $willkommen_bild_fur_hauptseite ?>" alt="Teaser"
                     height="200" width="250">
                <div class="welcome-teaser-text"><?php echo $willkommen_text_fur_hauptseite ?></div>
            </div>
            <div class="welcome-teaser-info">
                <?php echo $willkommen_beschreibung_fur_hauptseite ?>
            </div>
        </a>
        <a href="<?php echo $willkommen_link_fur_therapie ?>" class="welcome-teaser-container">
            <div class="welcome-teaser">
                <img class="welcome-teaser-image"
                     src="<?php echo $willkommen_bild_fur_therapie ?>" alt="Teaser"
                     height="200" width="250">
                <div class="welcome-teaser-text"><?php echo $willkommen_text_fur_therapie ?></div>
            </div>
            <div class="welcome-teaser-info">
                <?php echo $willkommen_beschreibung_fur_therapie ?>
            </div>
        </a>
        <a href="<?php echo $willkommen_link_fur_pflege ?>" class="welcome-teaser-container">
            <div class="welcome-teaser">
                <img class="welcome-teaser-image"
                     src="<?php echo $willkommen_bild_fur_pflege ?>" alt="Teaser"
                     height="200" width="250">
                <div class="welcome-teaser-text"><?php echo $willkommen_text_fur_pflege ?></div>
            </div>
            <div class="welcome-teaser-info">
                <?php echo $willkommen_beschreibung_fur_pflege ?>
            </div>
        </a>
        <a href="<?php echo $willkommen_link_fur_news ?>" class="welcome-teaser-container">
            <div class="welcome-teaser">
                <img class="welcome-teaser-image"
                     src="<?php echo $willkommen_bild_fur_news ?>" alt="Teaser"
                     height="200" width="250">
                <div class="welcome-teaser-text"><?php echo $willkommen_text_fur_news ?></div>
            </div>
            <div class="welcome-teaser-info">
                <?php echo $willkommen_beschreibung_fur_news ?>
            </div>
        </a>
        <a href="<?php echo $willkommen_link_fur_karriere ?>" class="welcome-teaser-container">
            <div class="welcome-teaser">
                <img class="welcome-teaser-image"
                     src="<?php echo $willkommen_bild_fur_karriere ?>" alt="Teaser"
                     height="200" width="250">
                <div class="welcome-teaser-text"><?php echo $willkommen_text_fur_karriere ?></div>
            </div>
            <div class="welcome-teaser-info">
                <?php echo $willkommen_beschreibung_fur_karriere ?>
            </div>
        </a>
        <a href="<?php echo $willkommen_link_fur_philosophie ?>" target="_blank" class="welcome-teaser-container">
            <div class="welcome-teaser">
                <img class="welcome-teaser-image"
                     src="<?php echo $willkommen_bild_fur_philosophie ?>" alt="Teaser"
                     height="200" width="250">
                <div class="welcome-teaser-text"><?php echo $willkommen_text_fur_philosophie ?></div>
            </div>
            <div class="welcome-teaser-info">
                <?php echo $willkommen_beschreibung_fur_philosophie ?>
            </div>
        </a>
    </div><!-- #main .wrapper -->
    <footer>
        <?php wp_nav_menu(array('theme_location' => 'social-menu')); ?>
        <div>
            <?php echo $willkommen_copyright ?>
            <a href="<?php echo $willkommen_link_zum_impressum ?>">&nbsp;<?php echo $willkommen_impressum ?></a>
        </div>
    </footer>
</div><!-- #page -->

<?php wp_footer(); ?>
</body>
</html>

<?php
function theme_enqueue_styles() {

    $parent_style = 'parent-style';

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style )
    );
}
load_theme_textdomain( 'twentytwelve', get_stylesheet_directory() . '/languages' );
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

// This theme uses wp_nav_menu() in two locations.
register_nav_menus( array(
    'primary' => __( 'Primary Navigation', 'twentytwelve' ),
    'secondary' => __('Secondary Navigation', 'twentytwelve')
) );
add_filter( 'the_job_location_map_link', 'remove_the_job_location_map_link' );
add_filter('login_errors',create_function('$a', "return 'Nicht erfolgreich!';"));

function remove_the_job_location_map_link( $link ) {
    return strip_tags( $link );
}

// Define your keys here
define( 'RECAPTCHA_SITE_KEY', '6Le10SIUAAAAAAA6CCEppQ3kbIAlAyZi7XrW1BFK' );
define( 'RECAPTCHA_SECRET_KEY', '6Le10SIUAAAAAOSyFMTY3mc65h0SHZLJ70hYWAMC' );

// Enqueue Google reCAPTCHA scripts
add_action( 'wp_enqueue_scripts', 'recaptcha_scripts' );

function recaptcha_scripts() {
    wp_enqueue_script( 'recaptcha', 'https://www.google.com/recaptcha/api.js' );
}

// Add reCAPTCHA to the job submission form
// If you disabled company fields, the submit_job_form_end hook can be used instead from version 1.24.1 onwards
add_action( 'submit_job_form_company_fields_end', 'recaptcha_field' );

function recaptcha_field() {
    ?>
    <fieldset>
        <label>Sicherheitsüberprüfung</label>
        <div class="field">
            <div class="g-recaptcha" data-sitekey="<?php echo RECAPTCHA_SITE_KEY; ?>"></div>
        </div>
    </fieldset>
    <?php
}

// Validate
add_filter( 'submit_job_form_validate_fields', 'validate_recaptcha_field' );

function validate_recaptcha_field( $success ) {
    $response = wp_remote_get( add_query_arg( array(
        'secret'   => RECAPTCHA_SECRET_KEY,
        'response' => isset( $_POST['g-recaptcha-response'] ) ? $_POST['g-recaptcha-response'] : '',
        'remoteip' => isset( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR']
    ), 'https://www.google.com/recaptcha/api/siteverify' ) );

    if ( is_wp_error( $response ) || empty( $response['body'] ) || ! ( $json = json_decode( $response['body'] ) ) || ! $json->success ) {
        return new WP_Error( 'validation-error', '"Ich bin kein Roboter" Prüfung fehlgeschlagen. Bitte wiederholen Sie den Versuch.' );
    }

    return $success;
}

function register_social_menu() {
    register_nav_menu('social-menu',__( 'Social Menu' ));
}
add_action( 'init', 'register_social_menu' );

add_action( 'job_application_form_fields_end', 'recaptcha_field' );
add_filter( 'application_form_validate_fields', 'validate_recaptcha_field' );

add_filter( 'storm_social_icons_hide_text', '__return_false' );
add_filter( 'storm_social_icons_type', create_function( '', 'return "icon-sign";' ) );

//Adding the Open Graph in the Language Attributes
function add_opengraph_doctype( $output ) {
	return $output . ' xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml"';
}
add_filter('language_attributes', 'add_opengraph_doctype');

//Lets add Open Graph Meta Info
function insert_fb_in_head() {
	global $post;
	if ( !is_singular()) //if it is not a post or a page
		return;
	echo '<meta property="fb:admins" content="616080171"/>';
	echo '<meta property="og:title" content="' . get_the_title() . '"/>';
	echo '<meta property="og:type" content="article"/>';
	echo '<meta property="og:url" content="' . get_permalink() . '"/>';
	echo '<meta property="og:site_name" content="Krohn-Leitmannstetter Webseite"/>';
	if(!has_post_thumbnail( $post->ID )) { //the post does not have featured image, use a default image
		$default_image="http://www.krohn-leitmannstetter.de/wp-content/themes/krohn-leitmannstetter/images/LogoGmbH.jpg"; //replace this with a default image on your server or an image in your media library
		echo '<meta property="og:image" content="' . $default_image . '"/>';
	}
	else{
		$thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium_large' );
		echo '<meta property="og:image" content="' . esc_attr( $thumbnail_src[0] ) . '"/>';
	}
	echo "
";
}
add_action( 'wp_head', 'insert_fb_in_head', 5 );

function limit_upload_size_limit_for_non_admin( $limit ) {
    return 16000000;
}

add_filter( 'upload_size_limit', 'limit_upload_size_limit_for_non_admin' );


function apply_wp_handle_upload_prefilter( $file ) {
        if ( $file['size'] > 16000000 ) {
            $file['error'] = __( 'Senden fehlgeschlagen. Die Bewerbungsunterlagen dürfen maximal 16 MB groß sein.', 'wp-job-manager' );
        }
    return $file;
}

add_filter( 'wp_handle_upload_prefilter', 'apply_wp_handle_upload_prefilter' );

?>
